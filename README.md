![Logo](https://github.com/gluester/StaffSySK/blob/master/staffsyssklogo.png)
# StaffSySK
A Skripted staff member ... skript!
### (!) Github.com/SkriptLang/Skript (!)
# Features
* Toggle building!
* Stop the server with a simple gui!
* Toggle chat!
* Hopefully, More coming!

# Requires
* Skript (Version unknown, however I know it will work on 2.4.1)
* TuSKe
* Possibly skQuery

# Thanks To ...
The Minehut community (Discord + Forums) **http://www.minehut.com** skUnity community (Discord + Forums + Parser) **http://www.skunity.com** Of course, Skript itself. **http://www.github.com/SkriptLang/Skript**
